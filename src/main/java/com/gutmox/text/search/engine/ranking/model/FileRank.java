package com.gutmox.text.search.engine.ranking.model;

import java.util.Objects;

public class FileRank {

    private String filename;

    private Integer percentage = 0;

    private Long occurrences = 0L;

    private Integer matchedWords = 0;

    public FileRank(String filename) {

        this.filename = filename;
    }

    public void increaseOccurrences(Long occurrences) {
        this.occurrences = this.occurrences + occurrences;
    }

    public void increaseMatchedWords() {
        matchedWords ++;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public String getFilename() {
        return filename;
    }

    public Long getOccurrences() {
        return occurrences;
    }

    public Integer getMatchedWords() {
        return matchedWords;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileRank fileRank = (FileRank) o;
        return Objects.equals(filename, fileRank.filename) &&
                Objects.equals(percentage, fileRank.percentage) &&
                Objects.equals(occurrences, fileRank.occurrences) &&
                Objects.equals(matchedWords, fileRank.matchedWords);
    }

    @Override
    public int hashCode() {

        return Objects.hash(filename, percentage, occurrences, matchedWords);
    }

    @Override
    public String toString() {
        return "FileRank{" +
                "filename='" + filename + '\'' +
                ", percentage=" + percentage +
                ", occurrences=" + occurrences +
                ", matchedWords=" + matchedWords +
                '}';
    }
}
