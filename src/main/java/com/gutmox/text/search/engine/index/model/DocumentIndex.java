package com.gutmox.text.search.engine.index.model;

import java.util.Objects;

public class DocumentIndex {

    private String documentName;

    private Long occurrences = 1L;

    public DocumentIndex(String documentName) {

        this.documentName = documentName;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void increaseOccurrences() {

        occurrences++;
    }

    public Long getOccurrences() {

        return occurrences;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocumentIndex that = (DocumentIndex) o;
        return Objects.equals(documentName, that.documentName) &&
                Objects.equals(occurrences, that.occurrences) ;
    }

    @Override
    public int hashCode() {

        return Objects.hash(documentName, occurrences);
    }

    @Override
    public String toString() {
        return "DocumentIndex{" +
                "documentName='" + documentName + '\'' +
                ", occurrences=" + occurrences +
                '}';
    }
}
