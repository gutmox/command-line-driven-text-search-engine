package com.gutmox.text.search.engine;

import com.gutmox.text.search.engine.exceptions.BadParameters;
import com.gutmox.text.search.engine.factories.TextSearchFactory;

public class TextSearchEngineApplication implements TextSearchFactory {

    private TextSearchFactory textSearchFactory;

    public TextSearchEngineApplication(TextSearchFactory textSearchFactory) {

        this.textSearchFactory = textSearchFactory;
    }

    public static void main(String[] args) {

        try {

            new TextSearchEngineApplication(new TextSearchFactory() {}).execute(args);

        } catch (BadParameters badParameters){
            System.exit(-1);
        }
    }

    public void execute(String[] args){

        validateInput(args);

        textSearchFactory.documentsScanner().processFolder(args[0]);

        textSearchFactory.commandLinePrompt().execute();
    }

    private void validateInput(String[] args) {

        if (args == null || args .length != 1){

            System.err.println("Documents directory must be passed as parameter");

            throw new BadParameters();
        }
    }
}
