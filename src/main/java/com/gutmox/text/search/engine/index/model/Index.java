package com.gutmox.text.search.engine.index.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Index {

    private Long occurrences;

    private List<DocumentIndex> documentsIndex;

    public Index() {

        occurrences = 1L;
        documentsIndex = new ArrayList<>();
    }

    public Long getOccurrences() {

        return occurrences;
    }

    public void setOccurrences(Long occurrences) {

        this.occurrences = occurrences;
    }

    public List<DocumentIndex> getDocumentsIndex() {

        return documentsIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Index index = (Index) o;
        return Objects.equals(occurrences, index.occurrences) &&
                Objects.equals(documentsIndex, index.documentsIndex);
    }

    @Override
    public int hashCode() {

        return Objects.hash(occurrences, documentsIndex);
    }

    @Override
    public String toString() {
        return "Index{" +
                "occurrences=" + occurrences +
                ", documentsIndex=" + documentsIndex +
                '}';
    }
}
