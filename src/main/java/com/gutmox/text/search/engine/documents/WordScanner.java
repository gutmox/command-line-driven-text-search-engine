package com.gutmox.text.search.engine.documents;

import com.gutmox.text.search.engine.index.Indexer;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class WordScanner {

    private static final Pattern SPECIAL_CHARACTERS = Pattern.compile("\\p{Punct}+");

    private Indexer indexer;

    private WordContentFilter wordContentFilter;

    public WordScanner(Indexer indexer, WordContentFilter wordContentFilter) {

        this.indexer = indexer;

        this.wordContentFilter = wordContentFilter;
    }

    public Consumer<? super String> execute(String fileName) {

        return line -> processLine(fileName, line);
    }


    private void processLine(String fileName, String line) {

        Arrays.stream(SPECIAL_CHARACTERS.matcher(line).replaceAll("").split(" "))
                .filter(word -> wordContentFilter.isActualWord(word))
                .forEach(word ->
                        indexer.addWord(fileName, word));
    }
}
