package com.gutmox.text.search.engine.documents;

import java.util.Arrays;
import java.util.List;

/**
 * For this initial version, I'm just filtering some articles and pronouns, but it'd be necessary
 * a dictionary with 'non content words'
 *
 */
public class WordContentFilter {

    List<String> articles = Arrays.asList("a", "an", "and", "the", "any", "in");

    List<String> pronouns = Arrays.asList("he", "she", "they", "it",
            "anybody", "anyone", "anything", "each", "either", "everybody", "everyone",
            "everything", "neither", "nobody", "no one", "nothing", "one", "somebody",
            "someone", "something", "both", "few", "many", "several", "all", "any", "most",
            "none", "some", "much", "such");

    public boolean isActualWord(String word) {

        return (!isanArticle(word) & !isaPronoun(word));
    }

    private boolean isanArticle(String word) {

        return articles.contains(word);
    }

    private boolean isaPronoun(String word) {

        return pronouns.contains(word);
    }
}
