package com.gutmox.text.search.engine.search;

import com.gutmox.text.search.engine.documents.WordContentFilter;
import com.gutmox.text.search.engine.index.Indexer;
import com.gutmox.text.search.engine.index.model.Index;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SearchEngine {

    private Indexer indexer;

    private WordContentFilter wordContentFilter;

    public SearchEngine(Indexer indexer, WordContentFilter wordContentFilter) {

        this.indexer = indexer;

        this.wordContentFilter = wordContentFilter;
    }

    public List<Optional<Index>> doSearch(String input) {

        List<Optional<Index>> collectedWords = Arrays.stream(input.split(" "))
                .filter(word -> wordContentFilter.isActualWord(word))
                .map(this::searchInIndexer).collect(Collectors.toList());

        return collectedWords;
    }

    private Optional<Index> searchInIndexer(String word) {

        Optional<Index> indexByWord = indexer.findIndexByWord(word);

        return indexByWord;
    }
}
