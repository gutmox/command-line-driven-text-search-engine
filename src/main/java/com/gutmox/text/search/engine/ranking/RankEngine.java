package com.gutmox.text.search.engine.ranking;

import com.gutmox.text.search.engine.index.model.DocumentIndex;
import com.gutmox.text.search.engine.index.model.Index;
import com.gutmox.text.search.engine.ranking.model.FileRank;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class RankEngine {


    public List<FileRank> doRank(List<Optional<Index>> searchResults) {

        Map<String, FileRank> ranking = new ConcurrentHashMap<>();

        searchResults.forEach(index -> index.ifPresent(getIndexConsumer(ranking)));

        List<FileRank> rankedResults = ranking.values().stream()
                .map(fileRank -> calculatePercentages(fileRank, searchResults.size()))
                .sorted((rank1, rank2) -> percentageDifference(rank1, rank2))
                .collect(Collectors.toList());

        return rankedResults;
    }

    private Integer percentageDifference(FileRank rank1, FileRank rank2) {

        return rank2.getPercentage() - rank1.getPercentage();
    }

    private FileRank calculatePercentages(FileRank fileRank, Integer searchedWordsNumber) {

        Integer percent = (fileRank.getMatchedWords() * 100) / searchedWordsNumber;

        fileRank.setPercentage(percent);

        return fileRank;
    }

    private Consumer<Index> getIndexConsumer(Map<String, FileRank> ranking) {

        return wordIndex ->
                wordIndex.getDocumentsIndex().forEach(getDocumentIndexConsumer(ranking));
    }

    private Consumer<DocumentIndex> getDocumentIndexConsumer(Map<String, FileRank> ranking) {

        return documentIndex -> {

            String documentName = documentIndex.getDocumentName();

            if (ranking.get(documentName) == null) {

                ranking.put(documentName,
                        new FileRank(documentName));
            }

            ranking.get(documentName).increaseMatchedWords();

            ranking.get(documentName).increaseOccurrences(documentIndex.getOccurrences());
        };
    }
}
