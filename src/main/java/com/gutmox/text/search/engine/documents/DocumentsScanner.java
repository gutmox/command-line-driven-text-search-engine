package com.gutmox.text.search.engine.documents;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class DocumentsScanner {

    private WordScanner wordScanner;

    public DocumentsScanner(WordScanner wordScanner) {

        this.wordScanner = wordScanner;
    }

    public void processFolder(String directory) {

        try (Stream<Path> paths = Files.walk(Paths.get(directory))) {

            final AtomicInteger count = new AtomicInteger();

            paths.filter(Files::isRegularFile)
                    .forEach(file -> {

                        count.incrementAndGet();

                        readDocumentContent(file, wordScanner.execute(file.getFileName().toFile().getName()));
                    });

            System.out.println(count.get() + " files read in directory " + directory);

        } catch (IOException e) {

            e.printStackTrace();
        }
    }


    private void readDocumentContent(Path fileName, Consumer<? super String> processWords) {

        try (Stream<String> stream = Files.lines(fileName)) {

            stream.forEach(processWords);

        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}
