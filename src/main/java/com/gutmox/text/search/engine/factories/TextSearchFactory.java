package com.gutmox.text.search.engine.factories;

import com.gutmox.text.search.engine.cli.CommandLinePrompt;
import com.gutmox.text.search.engine.documents.DocumentsScanner;
import com.gutmox.text.search.engine.documents.WordContentFilter;
import com.gutmox.text.search.engine.documents.WordScanner;
import com.gutmox.text.search.engine.index.Indexer;
import com.gutmox.text.search.engine.ranking.RankEngine;
import com.gutmox.text.search.engine.search.SearchEngine;

public interface TextSearchFactory {

    Indexer indexer = new Indexer();

    WordContentFilter wordContentFilter = new WordContentFilter();

    WordScanner WORD_SCANNER = new WordScanner(indexer, wordContentFilter);

    DocumentsScanner documentsScanner = new DocumentsScanner(WORD_SCANNER);

    SearchEngine searcher = new SearchEngine(indexer, wordContentFilter);

    RankEngine rankEngine = new RankEngine();

    CommandLinePrompt commandLinePrompt = new CommandLinePrompt(searcher, rankEngine);

    default CommandLinePrompt commandLinePrompt() {

        return commandLinePrompt;
    }

    default DocumentsScanner documentsScanner() {

        return documentsScanner;
    }
}
