package com.gutmox.text.search.engine.index;

import com.gutmox.text.search.engine.index.model.DocumentIndex;
import com.gutmox.text.search.engine.index.model.Index;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class Indexer {

    private Map<String, Index> indexMap = new ConcurrentHashMap<>();

    public void addWord(String fileName, String word) {

        String normalisedWord = normaliseWord(word);

        Index index = indexMap.get(normalisedWord);

        if (index == null) {

            index = new Index();

            index.getDocumentsIndex().add(new DocumentIndex(fileName));

        } else {

            index.setOccurrences(index.getOccurrences() + 1);

            Optional<DocumentIndex> documentIndexInMemory =
                    index.getDocumentsIndex().stream().filter(documentIndex ->
                            documentIndex.getDocumentName().equals(fileName))
                            .findFirst();

            if (!documentIndexInMemory.isPresent()) {

                index.getDocumentsIndex().add(new DocumentIndex(fileName));
            }

            documentIndexInMemory.ifPresent(documentIndex -> documentIndex.increaseOccurrences());
        }

        indexMap.put(normalisedWord, index);
    }

    private String normaliseWord(String word) {

        return word.toLowerCase();
    }


    public Optional<Index> findIndexByWord(String word){

        return Optional.ofNullable(indexMap.get(normaliseWord(word)));
    }
}
