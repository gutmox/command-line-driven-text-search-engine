package com.gutmox.text.search.engine.cli;

import com.gutmox.text.search.engine.index.model.Index;
import com.gutmox.text.search.engine.ranking.RankEngine;
import com.gutmox.text.search.engine.ranking.model.FileRank;
import com.gutmox.text.search.engine.search.SearchEngine;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class CommandLinePrompt {

    private static final String NO_MATCHES = "no matches found ";

    private static final String SEARCHING = "Searching ";

    private static final String QUIT = ":quit";

    private static String PROMPT = "search> ";

    private SearchEngine searchEngine;

    private RankEngine rankEngine;

    public CommandLinePrompt(SearchEngine searchEngine, RankEngine rankEngine) {

        this.searchEngine = searchEngine;

        this.rankEngine = rankEngine;
    }

    public void execute() {

        execute(new Scanner(System.in));
    }

    public void execute(Scanner input) {

        System.out.print(PROMPT);

        while (input.hasNext()) {

            String line = input.nextLine();

            if (line.equals(QUIT)) {

                break;
            }

            System.out.println(SEARCHING + line + " ...");

            List<Optional<Index>> searchResults = searchEngine.doSearch(line);

            printResult(searchResults);


            System.out.print(PROMPT);
        }
    }

    private void printResult(List<Optional<Index>> searchResults) {

        if (searchResults.size() == 0) {

            System.err.println(NO_MATCHES);

        } else {

            getRankResult(searchResults);
        }
    }

    private void getRankResult(List<Optional<Index>> searchResults) {

        List<FileRank> fileRanks = rankEngine.doRank(searchResults);

        if (fileRanks.size() == 0){

            System.err.println(NO_MATCHES);

        } else {

            printRankResults(fileRanks);
        }
    }

    private void printRankResults(List<FileRank> fileRanks) {

        fileRanks.forEach(fileRank -> {

            System.out.println(

                    fileRank.getFilename()
                            + "("
                            + fileRank.getMatchedWords()
                            + " occurrences)"
                            + " : "
                            + fileRank.getPercentage()
                            + "%"
            );
        });
    }
}
