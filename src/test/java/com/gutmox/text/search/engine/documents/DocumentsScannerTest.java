package com.gutmox.text.search.engine.documents;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.function.Consumer;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DocumentsScannerTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private DocumentsScanner documentsScanner;

    @Mock
    private WordScanner wordScanner;

    @Mock
    private Consumer<? super String> execution;

    @Before
    public void setUp(){

        System.setOut(new PrintStream(outContent));

        documentsScanner = new DocumentsScanner(wordScanner);
    }

    @Test
    public void test_should_get_system_out() {

        System.out.print("hello");

        assertThat(outContent.toString()).isEqualTo("hello");
    }

    @Test
    public void should_scan_all_files_in_directory() {

        doReturn(execution).when(wordScanner).execute(any());

        documentsScanner.processFolder("./src/test/resources/test");

        verify(wordScanner).execute("unit-tests.txt");
    }

    @Test
    public void should_get_number_of_files_in_directory_via_console() {

        doReturn(execution).when(wordScanner).execute(any());

        documentsScanner.processFolder("./src/test/resources/test");

        assertThat(outContent.toString().trim()).isEqualTo("1 files read in directory ./src/test/resources/test");
    }

    @After
    public void cleanUp() {

        System.setOut(null);
    }
}