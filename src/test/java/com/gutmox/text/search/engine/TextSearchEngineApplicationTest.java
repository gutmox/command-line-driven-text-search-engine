package com.gutmox.text.search.engine;

import com.gutmox.text.search.engine.cli.CommandLinePrompt;
import com.gutmox.text.search.engine.documents.DocumentsScanner;
import com.gutmox.text.search.engine.exceptions.BadParameters;
import com.gutmox.text.search.engine.factories.TextSearchFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TextSearchEngineApplicationTest {

    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private TextSearchEngineApplication textSearchEngineApplication;

    @Mock
    private TextSearchFactory textSearchFactory;

    @Mock
    private CommandLinePrompt commandLinePrompt;

    @Mock
    private DocumentsScanner documentsScanner;

    @Before
    public void setUp() {

        textSearchEngineApplication = new TextSearchEngineApplication(textSearchFactory);

        System.setErr(new PrintStream(errContent));
    }

    @Test

    public void test_should_get_system_err() {

        System.err.print("just checking err output");

        assertThat(errContent.toString()).isEqualTo("just checking err output");
    }

    @Test
    public void should_pass_a_directory_as_parameter() {

        try {

            textSearchEngineApplication.execute(null);

        } catch (BadParameters parameters) {

            assertThat(errContent.toString().trim()).isEqualTo("Documents directory must be passed as parameter");
        }
    }

    @Test
    public void should_invoke_factory_for_documents_scanning() {

        doReturn(documentsScanner).when(textSearchFactory).documentsScanner();

        doReturn(commandLinePrompt).when(textSearchFactory).commandLinePrompt();

        textSearchEngineApplication.execute(new String[]{"my_path"});

        verify(documentsScanner).processFolder("my_path");
    }

    @Test
    public void should_invoke_factory_for_command_line_prompt_executor() {

        doReturn(documentsScanner).when(textSearchFactory).documentsScanner();

        doReturn(commandLinePrompt).when(textSearchFactory).commandLinePrompt();

        textSearchEngineApplication.execute(new String[]{"my_path"});

        verify(commandLinePrompt).execute();
    }

    @After
    public void cleanUp() {

        System.setErr(null);
    }
}