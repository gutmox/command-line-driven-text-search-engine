package com.gutmox.text.search.engine.index;

import com.gutmox.text.search.engine.index.model.DocumentIndex;
import com.gutmox.text.search.engine.index.model.Index;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class IndexerTest {

    private Indexer indexer = new Indexer();

    private Index index;

    private DocumentIndex documentIndex;

    @Before
    public void setUp() {

        index = new Index();

        documentIndex = new DocumentIndex("file.txt");

        index.getDocumentsIndex().add(documentIndex);
    }

    @Test
    public void should_add_word_to_index() {

        indexer.addWord("file.txt", "word");

        assertThat(indexer.findIndexByWord("word").get()).isEqualTo(index);
    }

    @Test
    public void should_add_several_words_to_index() {

        Random random = new Random();

        int max = 1000;

        int min = 1;

        int number = random.nextInt(max - min + 1);

        index.setOccurrences((long) number);

        for (int i = 1; i < number; i++) {
            documentIndex.increaseOccurrences();
        }

        for (int i = 0; i < number; i++) {
            indexer.addWord("file.txt", "word");
        }

        assertThat(indexer.findIndexByWord("word").get()).isEqualTo(index);
    }

    @Test
    public void should_add_word_to_index_from_different_files() {

        index.setOccurrences(2L);
        documentIndex = new DocumentIndex("file2.txt");

        index.getDocumentsIndex().add(documentIndex);

        indexer.addWord("file.txt", "word");

        indexer.addWord("file2.txt", "word");

        assertThat(indexer.findIndexByWord("word").get()).isEqualTo(index);
    }

    @Test
    public void should_normalise_and_add_word_to_index() {

        indexer.addWord("file.txt", "WORD");

        assertThat(indexer.findIndexByWord("word").get()).isEqualTo(index);

    }
}