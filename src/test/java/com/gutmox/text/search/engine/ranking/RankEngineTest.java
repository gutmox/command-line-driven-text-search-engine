package com.gutmox.text.search.engine.ranking;

import com.gutmox.text.search.engine.index.model.DocumentIndex;
import com.gutmox.text.search.engine.index.model.Index;
import com.gutmox.text.search.engine.ranking.model.FileRank;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class RankEngineTest {

    private RankEngine rankEngine = new RankEngine();

    private Index index;

    private List<Optional<Index>> indexes;

    private DocumentIndex documentIndex;

    private FileRank fileRank;

    @Before
    public void setUp() {

        index = new Index();

        documentIndex = new DocumentIndex("file.txt");

        index.getDocumentsIndex().add(documentIndex);

        indexes = new ArrayList<>();

        indexes.add(Optional.ofNullable(index));

        fileRank = new FileRank("file.txt");


        fileRank.increaseOccurrences(1L);

        fileRank.increaseMatchedWords();
    }

    @Test
    public void should_rank_100_1_file() {

        fileRank.setPercentage(100);

        List<FileRank> fileRanks = rankEngine.doRank(indexes);

        assertThat(fileRanks.get(0)).isEqualTo(fileRank);
    }

    @Test
    public void should_rank_50_in_not_appear_in_1_file() {

        fileRank.setPercentage(50);

        DocumentIndex documentIndex = new DocumentIndex("file2.txt");

        Index index = new Index();

        index.getDocumentsIndex().add(documentIndex);

        indexes.add(Optional.ofNullable(index));

        List<FileRank> fileRanks = rankEngine.doRank(indexes);

        assertThat(fileRanks.get(0)).isEqualTo(fileRank);
    }

    @Test
    public void should_rank_0_in_not_appear_in_0_file() {

        List<Optional<Index>> indexes = new ArrayList<>();

        indexes.add(Optional.empty());

        List<FileRank> fileRanks = rankEngine.doRank(indexes);

        assertThat(fileRanks.size()).isEqualTo(0);
    }
}