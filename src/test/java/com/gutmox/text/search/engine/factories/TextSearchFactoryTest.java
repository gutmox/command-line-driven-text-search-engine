package com.gutmox.text.search.engine.factories;

import com.gutmox.text.search.engine.cli.CommandLinePrompt;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TextSearchFactoryTest {

    private TextSearchFactory textSearchFactory;

    @Before
    public void setUp(){

        textSearchFactory = new TextSearchFactory() {};
    }

    @Test
    public void command_line_prompt_should_have_all_singletons() {

        assertThat(textSearchFactory).isNotNull();

        CommandLinePrompt commandLinePrompt = textSearchFactory.commandLinePrompt();

        assertThat(commandLinePrompt).isNotNull();

    }
}