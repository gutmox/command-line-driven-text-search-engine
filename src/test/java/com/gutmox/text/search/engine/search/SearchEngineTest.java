package com.gutmox.text.search.engine.search;

import com.gutmox.text.search.engine.documents.WordContentFilter;
import com.gutmox.text.search.engine.index.Indexer;
import com.gutmox.text.search.engine.index.model.Index;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class SearchEngineTest {

    private SearchEngine searchEngine;

    @Mock
    private Indexer indexer;

    @Mock
    private WordContentFilter wordContentFilter;

    private Index index;


    @Before
    public void setUp(){

        searchEngine = new SearchEngine(indexer, wordContentFilter);

        index = new Index();
    }

    @Test
    public void should_find_word_in_index() {

        doReturn(true).when(wordContentFilter).isActualWord("word1");

        doReturn(Optional.ofNullable(index)).when(indexer).findIndexByWord("word1");

        List<Optional<Index>> searchResult = searchEngine.doSearch("word1");

        assertThat(searchResult.size()).isEqualTo(1);

        assertThat(searchResult.get(0).get()).isEqualTo(index);
    }

    @Test
    public void should_filter_non_content_words() {

        doReturn(false).when(wordContentFilter).isActualWord("she");

        List<Optional<Index>> searchResult = searchEngine.doSearch("she");

        assertThat(searchResult.size()).isEqualTo(0);
    }
}