package com.gutmox.text.search.engine.documents;

import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class WordContentFilterTest {

    private WordContentFilter wordContentFilter = new WordContentFilter();

    @Test
    public void articles_shouldnt_be_content_words(){

        Arrays.asList("a", "an", "and", "the", "any", "in").forEach(word ->
            assertThat(wordContentFilter.isActualWord(word)).isFalse()
        );
    }

    @Test
    public void pronouns_shouldnt_be_content_words(){

        Arrays.asList("he", "she", "they", "it",
                "anybody", "anyone", "anything", "each", "either", "everybody", "everyone",
                "everything", "neither", "nobody", "no one", "nothing", "one", "somebody",
                "someone", "something", "both", "few", "many", "several", "all", "any", "most",
                "none", "some", "much", "such").forEach(word ->
                assertThat(wordContentFilter.isActualWord(word)).isFalse()
        );
    }
}