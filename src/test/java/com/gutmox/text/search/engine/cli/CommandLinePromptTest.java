package com.gutmox.text.search.engine.cli;

import com.gutmox.text.search.engine.index.model.Index;
import com.gutmox.text.search.engine.ranking.RankEngine;
import com.gutmox.text.search.engine.ranking.model.FileRank;
import com.gutmox.text.search.engine.search.SearchEngine;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CommandLinePromptTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private CommandLinePrompt commandLinePrompt;

    @Mock
    private SearchEngine searchEngine;

    @Mock
    private RankEngine rankEngine;

    private String words = "word1 word2";

    private InputStream anyInputStream;

    @Mock
    private List<Optional<Index>> searchResults;

    private List<FileRank> fileRanks = new ArrayList<>();

    @Before
    public void setUp(){

        System.setOut(new PrintStream(outContent));

        System.setErr(new PrintStream(errContent));

        anyInputStream = new ByteArrayInputStream(words.getBytes());

        FileRank fileRank = new FileRank("file.txt");

        fileRank.setPercentage(100);

        fileRank.increaseOccurrences(1L);

        fileRank.increaseMatchedWords();

        fileRanks.add(fileRank);
        
        commandLinePrompt = new CommandLinePrompt(searchEngine, rankEngine);
    }

    @Test
    public void should_propagate_words_to_search_engine() {

        Scanner input = new Scanner(anyInputStream);

        commandLinePrompt.execute(input);

        verify(searchEngine).doSearch(words);
    }

    @Test
    public void should_prompt_words_before_search() {

        Scanner input = new Scanner(anyInputStream);

        commandLinePrompt.execute(input);

        assertThat(outContent.toString()).isEqualTo("search> Searching word1 word2 ...\n" +
                "search> ");
    }

    @Test
    public void should_prompt_no_matches_when_no_results() throws IOException {

        Scanner input = new Scanner(anyInputStream);

        commandLinePrompt.execute(input);

        assertThat(errContent.toString().trim()).isEqualTo("no matches found");
    }

    @Test
    public void should_prompt_results(){

        Scanner input = new Scanner(anyInputStream);

        doReturn(searchResults).when(searchEngine).doSearch(words);

        doReturn(1).when(searchResults).size();

        doReturn(fileRanks).when(rankEngine).doRank(searchResults);
        
        commandLinePrompt.execute(input);

        assertThat(outContent.toString()).isEqualTo("search> Searching word1 word2 ...\n" +
                "file.txt(1 occurrences) : 100%\n" +
                "search> ");
    }

    @Test
    public void should_exit_when_quit_command(){

        ByteArrayInputStream quitIputStream = new ByteArrayInputStream(":quit".getBytes());

        Scanner input = new Scanner(quitIputStream);

        commandLinePrompt.execute(input);

        assertThat(outContent.toString()).isEqualTo("search> ");

        assertThat(errContent.toString()).isEqualTo("");
    }
}