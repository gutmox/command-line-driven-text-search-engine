package com.gutmox.text.search.engine.documents;

import com.gutmox.text.search.engine.index.Indexer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class WordScannerTest {

    private WordScanner wordScanner;

    @Mock
    private Indexer indexer;

    @Mock
    private WordContentFilter wordContentFilter;

    @Before
    public void setUp(){

        wordScanner = new WordScanner(indexer, wordContentFilter);
    }

    @Test
    public void should_add_line_words_to_indexer() {

        doReturn(true).when(wordContentFilter).isActualWord(any());

        Arrays.asList("boys steadily monotonously stream").stream().forEach(
                wordScanner.execute("unit-tests.txt"));

        Arrays.asList("boys", "steadily", "monotonously", "stream").forEach(word ->{

            verify(indexer).addWord("unit-tests.txt", word);
        });
    }

    @Test
    public void should_ignore_special_characters() {

        doReturn(true).when(wordContentFilter).isActualWord(any());

        Arrays.asList("boys. steadily, monotonously; stream-").stream().forEach(
                wordScanner.execute("unit-tests.txt"));

        Arrays.asList("boys", "steadily", "monotonously", "stream").forEach(word ->{

            verify(indexer).addWord("unit-tests.txt", word);
        });
    }
}