## Command line driven text search engine
 
This project aim is to write a command line driven text search engine

This tool reads all the text files in the given directory, 
building an in memory representation of the files and their contents, 
and then give a command prompt at which interactive searches can be performed.

The search take the words given on the command prompt and return a list of the top 
matching filenames in rank order, giving the rank score against each match.

#### Ranking

- The rank score is 100% if a file contains all the words
- It is 0% if it contains none of the words
- It is between 0 and 100 when contains some of the words


#### How to build it 

```
gradle clean build
```

#### How to run it 

```
java -jar build/libs/text-search-engine.jar ${path_to_txt_files}
```

#### How to use it all in one 

From whenever this project has been cloned: 

```
$ gradle clean build && java ­jar java -jar build/libs/text-search-engine.jar ./src/test/resources/gutemberg
6 files read in directory ./src/test/resources 

search> to be or not to be
filename1 : 100% 
filename2 : 95% 
search>
search> cats
no matches found 
search> :quit

```

#### Testing Data

For testing purposes, I am using free classic books from http://www.gutenberg.org:

###### With Roberts to Pretoria
```
http://www.gutenberg.org/cache/epub/56143/pg56143.txt
```
###### A Tale of Two Cities, by Charles Dickens:

```
https://www.gutenberg.org/files/98/98-0.txt
```
###### Dracula, by Bram Stoker
```
http://www.gutenberg.org/cache/epub/345/pg345.txt
```

###### Grimms’ Fairy Tales
```
https://www.gutenberg.org/files/2591/2591-0.txt
```
###### The Adventures of Tom Sawyer
```
https://www.gutenberg.org/files/74/74-0.txt
```
###### Adventures of Huckleberry Finn
```
https://www.gutenberg.org/files/76/76-0.txt
```
